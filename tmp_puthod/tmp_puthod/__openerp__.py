# -*- coding: utf-8 -*-
{
    'name': "tmp_puthod",

    'summary': """
        Darbtech's module to import tmp_puthod Data""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Darbtech",
    'website': "https://darbtech.net",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','product'],

    # always loaded
    'data': [
        'views/tmpputhod.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
}
